package org.abh.common.database.query;

public interface SqlRepresentation {
	public void getSql(StringBuilder buffer);
}
