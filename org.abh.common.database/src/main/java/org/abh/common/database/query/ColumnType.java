package org.abh.common.database.query;

public enum ColumnType {
	INT,
	DOUBLE,
	STRING
}
