package org.abh.common.database.query;

public enum SortOrder {
	ASC,
	DESC
}
